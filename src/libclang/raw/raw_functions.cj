package libclang.raw

import libclang.clangmacro.*

public type CXIndex = CPointer<Unit>
@FromClang["5.0"](public type CXTargetInfo = CPointer<Unit>) // actually CPointer<CXTargetInfoImpl>
public type CXTranslationUnit = CPointer<Unit> // actually CPointer<CXTranslationUnit>
public type CXClientData = CPointer<Unit>
public type CXFile = CPointer<Unit>
public type CXDiagnostic = CPointer<Unit>
public type CXDiagnosticSet = CPointer<Unit>
public type CXCursorSet = CPointer<Unit>
public type CXCursorVisitor = CFunc<(CXCursor, CXCursor, CXClientData) -> UInt32>
@FromClang["7.0"](public type CXPrintingPolicy = CPointer<Unit>)
public type CXModule = CPointer<Unit>
public type CXCompletionString = CPointer<Unit>
public type CXInclusionVisitor = CFunc<(CXFile, CPointer<CXSourceLocation>, UInt32, CXClientData) -> Unit>
@FromClang["3.9"](public type CXEvalResult = CPointer<Unit>)
public type CXRemapping = CPointer<Unit>
public type CXIdxClientFile = CPointer<Unit>
public type CXIdxClientEntity = CPointer<Unit>
public type CXIdxClientContainer = CPointer<Unit>
public type CXIdxClientASTFile = CPointer<Unit>
public type CXIndexAction = CPointer<Unit>
@FromClang["3.7"](public type CXFieldVisitor = CFunc<(CXCursor, CXClientData) -> UInt32>)

foreign func clang_getCString(string: CXString): CString
foreign func clang_disposeString(string: CXString): Unit
@FromClang["3.8"]
foreign func clang_disposeStringSet(set: CPointer<CXStringSet>): Unit
// foreign func clang_getBuildSessionTimestamp(): UInt64
@FromClang["3.7"]
foreign func clang_free(buffer: CPointer<Unit>): Unit
foreign func clang_createIndex(excludeDeclarationsFromPCH: Int32, displayDiagnostics: Int32): CXIndex
foreign func clang_disposeIndex(index: CXIndex): Unit
foreign func clang_CXIndex_setGlobalOptions(index: CXIndex, options: UInt32): Unit
foreign func clang_CXIndex_getGlobalOptions(index: CXIndex): UInt32
@FromClang["6.0"]
foreign func clang_CXIndex_setInvocationEmissionPathOption(index: CXIndex, Path: CString): Unit
foreign func clang_getFileName(file: CXFile): CXString
// foreign func clang_getFileTime(file: CXFile): time_t // TODO: declare after time_t
foreign func clang_getFileUniqueID(file: CXFile, outID: CPointer<CXFileUniqueID>): Int32
foreign func clang_isFileMultipleIncludeGuarded(tu: CXTranslationUnit, file: CXFile): UInt32
foreign func clang_getFile(tu: CXTranslationUnit, file_name: CString): CXFile
foreign func clang_getFileContents(tu: CXTranslationUnit, file: CXFile, size: CPointer<Int32>): CString
@FromClang["3.6"]
foreign func clang_File_isEqual(file1: CXFile, file2: CXFile): UInt32
@FromClang["7.0"]
foreign func clang_File_tryGetRealPathName(file: CXFile): CXString
foreign func clang_getNullLocation(): CXSourceLocation
foreign func clang_equalLocations(loc1: CXSourceLocation, loc2: CXSourceLocation): UInt32
foreign func clang_getLocation(tu: CXTranslationUnit, file: CXFile, line: UInt32, column: UInt32): CXSourceLocation
foreign func clang_getLocationForOffset(tu: CXTranslationUnit, file: CXFile, offset: UInt32): CXSourceLocation
foreign func clang_Location_isInSystemHeader(location: CXSourceLocation): Int32
foreign func clang_Location_isFromMainFile(location: CXSourceLocation): Int32
foreign func clang_getNullRange(): CXSourceRange
foreign func clang_getRange(begin: CXSourceLocation, end: CXSourceLocation): CXSourceRange
foreign func clang_equalRanges(range1: CXSourceRange, range2: CXSourceRange): UInt32
foreign func clang_Range_isNull(range: CXSourceRange): Int32
foreign func clang_getExpansionLocation(
    location: CXSourceLocation,
    file: CPointer<CXFile>,
    line: CPointer<UInt32>,
    column: CPointer<UInt32>,
    offset: CPointer<UInt32>
): Unit
foreign func clang_getPresumedLocation(
    location: CXSourceLocation,
    filename: CPointer<CXString>,
    line: CPointer<UInt32>,
    column: CPointer<UInt32>
): Unit
foreign func clang_getInstantiationLocation(
    location: CXSourceLocation,
    file: CPointer<CXFile>,
    line: CPointer<UInt32>,
    column: CPointer<UInt32>,
    offset: CPointer<UInt32>
): Unit
foreign func clang_getSpellingLocation(
    location: CXSourceLocation,
    file: CPointer<CXFile>,
    line: CPointer<UInt32>,
    column: CPointer<UInt32>,
    offset: CPointer<UInt32>
): Unit
foreign func clang_getFileLocation(
    location: CXSourceLocation,
    file: CPointer<CXFile>,
    line: CPointer<UInt32>,
    column: CPointer<UInt32>,
    offset: CPointer<UInt32>
): Unit
foreign func clang_getRangeStart(range: CXSourceRange): CXSourceLocation
foreign func clang_getRangeEnd(range: CXSourceRange): CXSourceLocation
foreign func clang_getSkippedRanges(tu: CXTranslationUnit, file: CXFile): CPointer<CXSourceRangeList>
@FromClang["4.0"]
foreign func clang_getAllSkippedRanges(tu: CXTranslationUnit): CPointer<CXSourceRangeList>
foreign func clang_disposeSourceRangeList(ranges: CPointer<CXSourceRangeList>): Unit
foreign func clang_getNumDiagnosticsInSet(Diags: CXDiagnosticSet): UInt32
foreign func clang_getDiagnosticInSet(Diags: CXDiagnosticSet, Index: UInt32): CXDiagnostic
foreign func clang_loadDiagnostics(
    file: CString,
    error: CPointer<UInt32>,
    errorString: CPointer<CXString>
): CXDiagnosticSet
foreign func clang_disposeDiagnosticSet(Diags: CXDiagnosticSet): Unit
foreign func clang_getChildDiagnostics(D: CXDiagnostic): CXDiagnosticSet
foreign func clang_getNumDiagnostics(unit: CXTranslationUnit): UInt32
foreign func clang_getDiagnostic(unit: CXTranslationUnit, Index: UInt32): CXDiagnostic
foreign func clang_getDiagnosticSetFromTU(unit: CXTranslationUnit): CXDiagnosticSet
foreign func clang_disposeDiagnostic(Diagnostic: CXDiagnostic): Unit
foreign func clang_formatDiagnostic(Diagnostic: CXDiagnostic, Options: UInt32): CXString
foreign func clang_defaultDiagnosticDisplayOptions(): UInt32
foreign func clang_getDiagnosticSeverity(_: CXDiagnostic): UInt32
foreign func clang_getDiagnosticSpelling(_: CXDiagnostic): CXString
foreign func clang_getDiagnosticOption(Diag: CXDiagnostic, Disable: CPointer<CXString>): CXString
foreign func clang_getDiagnosticCategory(_: CXDiagnostic): UInt32
foreign func clang_getDiagnosticCategoryName(Category: UInt32): CXString
foreign func clang_getDiagnosticCategoryText(_: CXDiagnostic): CXString
foreign func clang_getDiagnosticNumRanges(_: CXDiagnostic): UInt32
foreign func clang_getDiagnosticRange(Diagnostic: CXDiagnostic, Range: UInt32): CXSourceRange
foreign func clang_getDiagnosticNumFixIts(Diagnostic: CXDiagnostic): UInt32
foreign func clang_getDiagnosticFixIt(
    Diagnostic: CXDiagnostic,
    FixIt: UInt32,
    ReplacementRange: CPointer<CXSourceRange>
): CXString
foreign func clang_getTranslationUnitSpelling(CTUnit: CXTranslationUnit): CXString
foreign func clang_createTranslationUnitFromSourceFile(
    CIdx: CXIndex,
    source_filename: CString,
    num_clang_command_line_args: Int32,
    clang_command_line_args: CPointer<CString>,
    num_unsaved_files: UInt32,
    unsaved_files: CPointer<CXUnsavedFile>
): CXTranslationUnit
foreign func clang_createTranslationUnit(CIdx: CXIndex, ast_filename: CString): CXTranslationUnit
foreign func clang_createTranslationUnit2(
    CIdx: CXIndex,
    ast_filename: CString,
    out_TU: CPointer<CXTranslationUnit>
): UInt32
foreign func clang_defaultEditingTranslationUnitOptions(): UInt32
foreign func clang_parseTranslationUnit(
    CIdx: CXIndex,
    source_filename: CString,
    command_line_args: CPointer<CString>,
    num_command_line_args: Int32,
    unsaved_files: CPointer<CXUnsavedFile>,
    num_unsaved_files: UInt32,
    options: UInt32
): CXTranslationUnit
foreign func clang_parseTranslationUnit2(
    CIdx: CXIndex,
    source_filename: CString,
    command_line_args: CPointer<CString>,
    num_command_line_args: Int32,
    unsaved_files: CPointer<CXUnsavedFile>,
    num_unsaved_files: UInt32,
    options: UInt32,
    out_TU: CPointer<CXTranslationUnit>
): UInt32
@FromClang["3.8"]
foreign func clang_parseTranslationUnit2FullArgv(
    CIdx: CXIndex,
    source_filename: CString,
    command_line_args: CPointer<CString>,
    num_command_line_args: Int32,
    unsaved_files: CPointer<CXUnsavedFile>,
    num_unsaved_files: UInt32,
    options: UInt32,
    out_TU: CPointer<CXTranslationUnit>
): UInt32
foreign func clang_defaultSaveOptions(TU: CXTranslationUnit): UInt32
foreign func clang_saveTranslationUnit(TU: CXTranslationUnit, FileName: CString, options: UInt32): Int32
@FromClang["5.0"]
foreign func clang_suspendTranslationUnit(_: CXTranslationUnit): UInt32
foreign func clang_disposeTranslationUnit(_: CXTranslationUnit): Unit
foreign func clang_defaultReparseOptions(TU: CXTranslationUnit): UInt32
foreign func clang_reparseTranslationUnit(
    TU: CXTranslationUnit,
    num_unsaved_files: UInt32,
    unsaved_files: CPointer<CXUnsavedFile>,
    options: UInt32
): Int32
foreign func clang_getTUResourceUsageName(kind: UInt32): CString
foreign func clang_getCXTUResourceUsage(TU: CXTranslationUnit): CXTUResourceUsage
foreign func clang_disposeCXTUResourceUsage(usage: CXTUResourceUsage): Unit
@FromClang["5.0"]
foreign func clang_getTranslationUnitTargetInfo(CTUnit: CXTranslationUnit): CXTargetInfo
@FromClang["5.0"]
foreign func clang_TargetInfo_dispose(Info: CXTargetInfo): Unit
@FromClang["5.0"]
foreign func clang_TargetInfo_getTriple(Info: CXTargetInfo): CXString
@FromClang["5.0"]
foreign func clang_TargetInfo_getPointerWidth(Info: CXTargetInfo): Int32
foreign func clang_getNullCursor(): CXCursor
foreign func clang_getTranslationUnitCursor(_: CXTranslationUnit): CXCursor
foreign func clang_equalCursors(_: CXCursor, _: CXCursor): UInt32
foreign func clang_Cursor_isNull(cursor: CXCursor): Int32
foreign func clang_hashCursor(_: CXCursor): UInt32
foreign func clang_getCursorKind(_: CXCursor): UInt32 /* CXCursorKind */
foreign func clang_isDeclaration(_: UInt32 /* CXCursorKind */): UInt32
@FromClang["7.0"]
foreign func clang_isInvalidDeclaration(_: CXCursor): UInt32
foreign func clang_isReference(_: UInt32 /* CXCursorKind */): UInt32
foreign func clang_isExpression(_: UInt32 /* CXCursorKind */): UInt32
foreign func clang_isStatement(_: UInt32 /* CXCursorKind */): UInt32
foreign func clang_isAttribute(_: UInt32 /* CXCursorKind */): UInt32
@FromClang["3.9"]
foreign func clang_Cursor_hasAttrs(C: CXCursor): UInt32
foreign func clang_isInvalid(_: UInt32 /* CXCursorKind */): UInt32
foreign func clang_isTranslationUnit(_: UInt32 /* CXCursorKind */): UInt32
foreign func clang_isPreprocessing(_: UInt32 /* CXCursorKind */): UInt32
foreign func clang_isUnexposed(_: UInt32 /* CXCursorKind */): UInt32
foreign func clang_getCursorLinkage(cursor: CXCursor): UInt32 /* CXCursorKind */
@FromClang["3.8"]
foreign func clang_getCursorVisibility(cursor: CXCursor): UInt32 /* CXVisibilityKind */
foreign func clang_getCursorAvailability(cursor: CXCursor): UInt32 /* CXAvailabilityKind */
foreign func clang_getCursorPlatformAvailability(
    cursor: CXCursor,
    always_deprecated: CPointer<Int32>,
    deprecated_message: CPointer<CXString>,
    always_unavailable: CPointer<Int32>,
    unavailable_message: CPointer<CXString>,
    availability: CPointer<CXPlatformAvailability>,
    availability_size: Int32
): Int32
foreign func clang_disposeCXPlatformAvailability(availability: CPointer<CXPlatformAvailability>): Unit
@FromClang["12.0"]
foreign func clang_Cursor_getVarDeclInitializer(cursor: CXCursor): CXCursor
@FromClang["12.0"]
foreign func clang_Cursor_hasVarDeclGlobalStorage(cursor: CXCursor): Int32
@FromClang["12.0"]
foreign func clang_Cursor_hasVarDeclExternalStorage(cursor: CXCursor): Int32
foreign func clang_getCursorLanguage(cursor: CXCursor): UInt32
@FromClang["6.0"]
foreign func clang_getCursorTLSKind(cursor: CXCursor): UInt32 /* CXTLSKind */
foreign func clang_Cursor_getTranslationUnit(_: CXCursor): CXTranslationUnit
foreign func clang_createCXCursorSet(): CXCursorSet
foreign func clang_disposeCXCursorSet(cset: CXCursorSet): Unit
foreign func clang_CXCursorSet_contains(cset: CXCursorSet, cursor: CXCursor): UInt32
foreign func clang_CXCursorSet_insert(cset: CXCursorSet, cursor: CXCursor): UInt32
foreign func clang_getCursorSemanticParent(cursor: CXCursor): CXCursor
foreign func clang_getCursorLexicalParent(cursor: CXCursor): CXCursor
foreign func clang_getOverriddenCursors(
    cursor: CXCursor,
    overridden: CPointer<CPointer<CXCursor>>,
    num_overridden: CPointer<UInt32>
): Unit
foreign func clang_disposeOverriddenCursors(overridden: CPointer<CXCursor>): Unit
foreign func clang_getIncludedFile(cursor: CXCursor): CXFile
foreign func clang_getCursor(_: CXTranslationUnit, _: CXSourceLocation): CXCursor
foreign func clang_getCursorLocation(_: CXCursor): CXSourceLocation
foreign func clang_getCursorExtent(_: CXCursor): CXSourceRange
foreign func clang_getCursorType(C: CXCursor): CXType
foreign func clang_getTypeSpelling(CT: CXType): CXString
foreign func clang_getTypedefDeclUnderlyingType(C: CXCursor): CXType
foreign func clang_getEnumDeclIntegerType(C: CXCursor): CXType
foreign func clang_getEnumConstantDeclValue(C: CXCursor): Int64
foreign func clang_getEnumConstantDeclUnsignedValue(C: CXCursor): UInt64
foreign func clang_getFieldDeclBitWidth(C: CXCursor): Int32
foreign func clang_Cursor_getNumArguments(C: CXCursor): Int32
foreign func clang_Cursor_getArgument(C: CXCursor, i: UInt32): CXCursor
@FromClang["3.6"]
foreign func clang_Cursor_getNumTemplateArguments(C: CXCursor): Int32
@FromClang["3.6"]
foreign func clang_Cursor_getTemplateArgumentKind(C: CXCursor, I: UInt32): UInt32
@FromClang["3.6"]
foreign func clang_Cursor_getTemplateArgumentType(C: CXCursor, I: UInt32): CXType
@FromClang["3.6"]
foreign func clang_Cursor_getTemplateArgumentValue(C: CXCursor, I: UInt32): Int64
@FromClang["3.6"]
foreign func clang_Cursor_getTemplateArgumentUnsignedValue(C: CXCursor, I: UInt32): UInt64
foreign func clang_equalTypes(A: CXType, B: CXType): UInt32
foreign func clang_getCanonicalType(T: CXType): CXType
foreign func clang_isConstQualifiedType(T: CXType): UInt32
@FromClang["3.9"]
foreign func clang_Cursor_isMacroFunctionLike(C: CXCursor): UInt32
@FromClang["3.9"]
foreign func clang_Cursor_isMacroBuiltin(C: CXCursor): UInt32
@FromClang["3.9"]
foreign func clang_Cursor_isFunctionInlined(C: CXCursor): UInt32
foreign func clang_isVolatileQualifiedType(T: CXType): UInt32
foreign func clang_isRestrictQualifiedType(T: CXType): UInt32
@FromClang["5.0"]
foreign func clang_getAddressSpace(T: CXType): UInt32
@FromClang["5.0"]
foreign func clang_getTypedefName(CT: CXType): CXString
foreign func clang_getPointeeType(T: CXType): CXType
foreign func clang_getTypeDeclaration(T: CXType): CXCursor
foreign func clang_getDeclObjCTypeEncoding(C: CXCursor): CXString
@FromClang["3.9"]
foreign func clang_Type_getObjCEncoding(ctype: CXType): CXString
foreign func clang_getTypeKindSpelling(K: UInt32): CXString
foreign func clang_getFunctionTypeCallingConv(T: CXType): UInt32
foreign func clang_getResultType(T: CXType): CXType
@FromClang["5.0"]
foreign func clang_getExceptionSpecificationType(T: CXType): Int32
foreign func clang_getNumArgTypes(T: CXType): Int32
foreign func clang_getArgType(T: CXType, i: UInt32): CXType
@FromClang["8.0"]
foreign func clang_Type_getObjCObjectBaseType(T: CXType): CXType
@FromClang["8.0"]
foreign func clang_Type_getNumObjCProtocolRefs(T: CXType): UInt32
@FromClang["8.0"]
foreign func clang_Type_getObjCProtocolDecl(T: CXType, i: UInt32): CXCursor
@FromClang["8.0"]
foreign func clang_Type_getNumObjCTypeArgs(T: CXType): UInt32
@FromClang["8.0"]
foreign func clang_Type_getObjCTypeArg(T: CXType, i: UInt32): CXType
foreign func clang_isFunctionTypeVariadic(T: CXType): UInt32
foreign func clang_getCursorResultType(C: CXCursor): CXType
@FromClang["5.0"]
foreign func clang_getCursorExceptionSpecificationType(C: CXCursor): Int32
foreign func clang_isPODType(T: CXType): UInt32
foreign func clang_getElementType(T: CXType): CXType
foreign func clang_getNumElements(T: CXType): Int64
foreign func clang_getArrayElementType(T: CXType): CXType
foreign func clang_getArraySize(T: CXType): Int64
@FromClang["3.9"]
foreign func clang_Type_getNamedType(T: CXType): CXType
@FromClang["5.0"]
foreign func clang_Type_isTransparentTagTypedef(T: CXType): UInt32
@FromClang["8.0"]
foreign func clang_Type_getNullability(T: CXType): UInt32
foreign func clang_Type_getAlignOf(T: CXType): Int64
foreign func clang_Type_getClassType(T: CXType): CXType
foreign func clang_Type_getSizeOf(T: CXType): Int64
foreign func clang_Type_getOffsetOf(T: CXType, S: CString): Int64
@FromClang["8.0"]
foreign func clang_Type_getModifiedType(T: CXType): CXType
@FromClang["11.0"]
foreign func clang_Type_getValueType(CT: CXType): CXType
@FromClang["3.7"]
foreign func clang_Cursor_getOffsetOfField(C: CXCursor): Int64
@FromClang["3.7"]
foreign func clang_Cursor_isAnonymous(C: CXCursor): UInt32
@FromClang["9.0"]
foreign func clang_Cursor_isAnonymousRecordDecl(C: CXCursor): UInt32
@FromClang["9.0"]
foreign func clang_Cursor_isInlineNamespace(C: CXCursor): UInt32

foreign func clang_Type_getNumTemplateArguments(T: CXType): Int32
foreign func clang_Type_getTemplateArgumentAsType(T: CXType, i: UInt32): CXType
foreign func clang_Type_getCXXRefQualifier(T: CXType): UInt32
foreign func clang_Cursor_isBitField(C: CXCursor): UInt32
foreign func clang_isVirtualBase(_: CXCursor): UInt32

foreign func clang_getCXXAccessSpecifier(_: CXCursor): UInt32

@FromClang["3.6"]
foreign func clang_Cursor_getStorageClass(_: CXCursor): UInt32
foreign func clang_getNumOverloadedDecls(cursor: CXCursor): UInt32
foreign func clang_getOverloadedDecl(cursor: CXCursor, index: UInt32): CXCursor
foreign func clang_getIBOutletCollectionType(_: CXCursor): CXType

foreign func clang_visitChildren(parent: CXCursor, visitor: CXCursorVisitor, client_data: CXClientData): UInt32
foreign func clang_getCursorUSR(_: CXCursor): CXString
foreign func clang_constructUSR_ObjCClass(class_name: CString): CXString
foreign func clang_constructUSR_ObjCCategory(class_name: CString, category_name: CString): CXString
foreign func clang_constructUSR_ObjCProtocol(protocol_name: CString): CXString
foreign func clang_constructUSR_ObjCIvar(name: CString, classUSR: CXString): CXString
foreign func clang_constructUSR_ObjCMethod(name: CString, isInstanceMethod: UInt32, classUSR: CXString): CXString
foreign func clang_constructUSR_ObjCProperty(property: CString, classUSR: CXString): CXString
foreign func clang_getCursorSpelling(_: CXCursor): CXString
foreign func clang_Cursor_getSpellingNameRange(_: CXCursor, pieceIndex: UInt32, options: UInt32): CXSourceRange


@FromClang["7.0"]
foreign func clang_PrintingPolicy_getProperty(Policy: CXPrintingPolicy, Property: UInt32): UInt32
@FromClang["7.0"]
foreign func clang_PrintingPolicy_setProperty(Policy: CXPrintingPolicy, Property: UInt32, Value: UInt32): Unit
@FromClang["7.0"]
foreign func clang_getCursorPrintingPolicy(_: CXCursor): CXPrintingPolicy
@FromClang["7.0"]
foreign func clang_PrintingPolicy_dispose(Policy: CXPrintingPolicy): Unit
@FromClang["7.0"]
foreign func clang_getCursorPrettyPrinted(Cursor: CXCursor, Policy: CXPrintingPolicy): CXString
foreign func clang_getCursorDisplayName(_: CXCursor): CXString
foreign func clang_getCursorReferenced(_: CXCursor): CXCursor
foreign func clang_getCursorDefinition(_: CXCursor): CXCursor
foreign func clang_isCursorDefinition(_: CXCursor): UInt32
foreign func clang_getCanonicalCursor(_: CXCursor): CXCursor
foreign func clang_Cursor_getObjCSelectorIndex(_: CXCursor): Int32
foreign func clang_Cursor_isDynamicCall(C: CXCursor): Int32
foreign func clang_Cursor_getReceiverType(C: CXCursor): CXType

foreign func clang_Cursor_getObjCPropertyAttributes(C: CXCursor, reserved: UInt32): UInt32
@FromClang["8.0"]
foreign func clang_Cursor_getObjCPropertyGetterName(C: CXCursor): CXString
@FromClang["8.0"]
foreign func clang_Cursor_getObjCPropertySetterName(C: CXCursor): CXString

foreign func clang_Cursor_getObjCDeclQualifiers(C: CXCursor): UInt32
foreign func clang_Cursor_isObjCOptional(C: CXCursor): UInt32
foreign func clang_Cursor_isVariadic(C: CXCursor): UInt32
@FromClang["5.0"]
foreign func clang_Cursor_isExternalSymbol(
    C: CXCursor,
    language: CPointer<CXString>,
    definedIn: CPointer<CXString>,
    isGenerated: CPointer<UInt32>
): UInt32

foreign func clang_Cursor_getCommentRange(C: CXCursor): CXSourceRange
foreign func clang_Cursor_getRawCommentText(C: CXCursor): CXString
foreign func clang_Cursor_getBriefCommentText(C: CXCursor): CXString
@FromClang["3.6"]
foreign func clang_Cursor_getMangling(_: CXCursor): CXString
@FromClang["3.8"]
foreign func clang_Cursor_getCXXManglings(_: CXCursor): CPointer<CXStringSet>
@FromClang["6.0"]
foreign func clang_Cursor_getObjCManglings(_: CXCursor): CPointer<CXStringSet>


foreign func clang_Cursor_getModule(C: CXCursor): CXModule
foreign func clang_getModuleForFile(_: CXTranslationUnit, _: CXFile): CXModule
foreign func clang_Module_getASTFile(Module: CXModule): CXFile
foreign func clang_Module_getParent(Module: CXModule): CXModule
foreign func clang_Module_getName(Module: CXModule): CXString
foreign func clang_Module_getFullName(Module: CXModule): CXString
foreign func clang_Module_isSystem(Module: CXModule): Int32
foreign func clang_Module_getNumTopLevelHeaders(_: CXTranslationUnit, Module: CXModule): UInt32
foreign func clang_Module_getTopLevelHeader(_: CXTranslationUnit, Module: CXModule, Index: UInt32): CXFile
@FromClang["3.9"]
foreign func clang_CXXConstructor_isConvertingConstructor(C: CXCursor): UInt32
@FromClang["3.9"]
foreign func clang_CXXConstructor_isCopyConstructor(C: CXCursor): UInt32
@FromClang["3.9"]
foreign func clang_CXXConstructor_isDefaultConstructor(C: CXCursor): UInt32
@FromClang["3.9"]
foreign func clang_CXXConstructor_isMoveConstructor(C: CXCursor): UInt32
@FromClang["3.8"]
foreign func clang_CXXField_isMutable(C: CXCursor): UInt32
@FromClang["3.9"]
foreign func clang_CXXMethod_isDefaulted(C: CXCursor): UInt32
foreign func clang_CXXMethod_isPureVirtual(C: CXCursor): UInt32
foreign func clang_CXXMethod_isStatic(C: CXCursor): UInt32
foreign func clang_CXXMethod_isVirtual(C: CXCursor): UInt32
@FromClang["6.0"]
foreign func clang_CXXRecord_isAbstract(C: CXCursor): UInt32
@FromClang["5.0"]
foreign func clang_EnumDecl_isScoped(C: CXCursor): UInt32
foreign func clang_CXXMethod_isConst(C: CXCursor): UInt32
foreign func clang_getTemplateCursorKind(C: CXCursor): UInt32
foreign func clang_getSpecializedCursorTemplate(C: CXCursor): CXCursor
foreign func clang_getCursorReferenceNameRange(C: CXCursor, NameFlags: UInt32, PieceIndex: UInt32): CXSourceRange

foreign func clang_getToken(TU: CXTranslationUnit, Location: CXSourceLocation): CPointer<CXToken>
foreign func clang_getTokenKind(_: CXToken): UInt32
foreign func clang_getTokenSpelling(_: CXTranslationUnit, _: CXToken): CXString
foreign func clang_getTokenLocation(_: CXTranslationUnit, _: CXToken): CXSourceLocation
foreign func clang_getTokenExtent(_: CXTranslationUnit, _: CXToken): CXSourceRange
foreign func clang_tokenize(
    TU: CXTranslationUnit,
    Range: CXSourceRange,
    Tokens: CPointer<CPointer<CXToken>>,
    NumTokens: CPointer<UInt32>
): Unit
foreign func clang_annotateTokens(
    TU: CXTranslationUnit,
    Tokens: CPointer<CXToken>,
    NumTokens: UInt32,
    Cursors: CPointer<CXCursor>
): Unit
foreign func clang_disposeTokens(TU: CXTranslationUnit, Tokens: CPointer<CXToken>, NumTokens: UInt32): Unit
foreign func clang_getCursorKindSpelling(Kind: UInt32): CXString
foreign func clang_getDefinitionSpellingAndExtent(
    _: CXCursor,
    startBuf: CPointer<CString>,
    endBuf: CPointer<CString>,
    startLine: CPointer<UInt32>,
    startColumn: CPointer<UInt32>,
    endLine: CPointer<UInt32>,
    endColumn: CPointer<UInt32>
): Unit
foreign func clang_enableStackTraces(): Unit
foreign func clang_executeOnThread(
    fn: CFunc<(CPointer<Unit>) -> Unit>,
    user_data: CPointer<Unit>,
    stack_size: UInt32
): Unit

foreign func clang_getCompletionChunkKind(completion_string: CXCompletionString, chunk_number: UInt32): UInt32
foreign func clang_getCompletionChunkText(completion_string: CXCompletionString, chunk_number: UInt32): CXString
foreign func clang_getCompletionChunkCompletionString(
    completion_string: CXCompletionString,
    chunk_number: UInt32
): CXCompletionString
foreign func clang_getNumCompletionChunks(completion_string: CXCompletionString): UInt32
foreign func clang_getCompletionPriority(completion_string: CXCompletionString): UInt32
foreign func clang_getCompletionAvailability(completion_string: CXCompletionString): UInt32
foreign func clang_getCompletionNumAnnotations(completion_string: CXCompletionString): UInt32
foreign func clang_getCompletionAnnotation(completion_string: CXCompletionString, annotation_number: UInt32): CXString
foreign func clang_getCompletionParent(completion_string: CXCompletionString, kind: CPointer<UInt32>): CXString
foreign func clang_getCompletionBriefComment(completion_string: CXCompletionString): CXString
foreign func clang_getCursorCompletionString(cursor: CXCursor): CXCompletionString



@FromClang["7.0"]
foreign func clang_getCompletionNumFixIts(results: CPointer<CXCodeCompleteResults>, completion_index: UInt32): UInt32
@FromClang["7.0"]
foreign func clang_getCompletionFixIt(
    results: CPointer<CXCodeCompleteResults>,
    completion_index: UInt32,
    fixit_index: UInt32,
    replacement_range: CPointer<CXSourceRange>
): CXString

foreign func clang_defaultCodeCompleteOptions(): UInt32
foreign func clang_codeCompleteAt(
    TU: CXTranslationUnit,
    complete_filename: CString,
    complete_line: UInt32,
    complete_column: UInt32,
    unsaved_files: CPointer<CXUnsavedFile>,
    num_unsaved_files: UInt32,
    options: UInt32
): CPointer<CXCodeCompleteResults>
foreign func clang_sortCodeCompletionResults(Results: CPointer<CXCompletionResult>, NumResults: UInt32): Unit
foreign func clang_disposeCodeCompleteResults(Results: CPointer<CXCodeCompleteResults>): Unit
foreign func clang_codeCompleteGetNumDiagnostics(Results: CPointer<CXCodeCompleteResults>): UInt32
foreign func clang_codeCompleteGetDiagnostic(Results: CPointer<CXCodeCompleteResults>, Index: UInt32): CXDiagnostic
foreign func clang_codeCompleteGetContexts(Results: CPointer<CXCodeCompleteResults>): UInt64
foreign func clang_codeCompleteGetContainerKind(
    Results: CPointer<CXCodeCompleteResults>,
    IsIncomplete: CPointer<UInt32>
): UInt32
foreign func clang_codeCompleteGetContainerUSR(Results: CPointer<CXCodeCompleteResults>): CXString
foreign func clang_codeCompleteGetObjCSelector(Results: CPointer<CXCodeCompleteResults>): CXString
foreign func clang_getClangVersion(): CXString
foreign func clang_toggleCrashRecovery(isEnabled: UInt32): Unit


foreign func clang_getInclusions(
    tu: CXTranslationUnit,
    visitor: CXInclusionVisitor,
    client_data: CXClientData
): Unit

@FromClang["3.9"]
foreign func clang_Cursor_Evaluate(C: CXCursor): CXEvalResult
@FromClang["3.9"]
foreign func clang_EvalResult_getKind(E: CXEvalResult): UInt32
@FromClang["3.9"]
foreign func clang_EvalResult_getAsInt(E: CXEvalResult): Int32
@FromClang["4.0"]
foreign func clang_EvalResult_getAsLongLong(E: CXEvalResult): Int64
@FromClang["4.0"]
foreign func clang_EvalResult_isUnsignedInt(E: CXEvalResult): UInt32
@FromClang["4.0"]
foreign func clang_EvalResult_getAsUnsigned(E: CXEvalResult): UInt64
@FromClang["3.9"]
foreign func clang_EvalResult_getAsDouble(E: CXEvalResult): Float64
@FromClang["3.9"]
foreign func clang_EvalResult_getAsStr(E: CXEvalResult): CString
@FromClang["3.9"]
foreign func clang_EvalResult_dispose(E: CXEvalResult): Unit


foreign func clang_getRemappings(path: CString): CXRemapping
foreign func clang_getRemappingsFromFileList(filePaths: CPointer<CString>, numFiles: UInt32): CXRemapping
foreign func clang_remap_getNumFiles(_: CXRemapping): UInt32
foreign func clang_remap_getFilenames(
    _: CXRemapping,
    index: UInt32,
    original: CPointer<CXString>,
    transformed: CPointer<CXString>
): Unit
foreign func clang_remap_dispose(_: CXRemapping): Unit
foreign func clang_findReferencesInFile(cursor: CXCursor, file: CXFile, visitor: CXCursorAndRangeVisitor): UInt32
foreign func clang_findIncludesInFile(
    TU: CXTranslationUnit,
    file: CXFile,
    visitor: CXCursorAndRangeVisitor
): UInt32
foreign func clang_index_isEntityObjCContainerKind(_: UInt32): Int32
foreign func clang_index_getObjCContainerDeclInfo(_: CPointer<CXIdxDeclInfo>): CPointer<CXIdxObjCContainerDeclInfo>
foreign func clang_index_getObjCInterfaceDeclInfo(_: CPointer<CXIdxDeclInfo>): CPointer<CXIdxObjCInterfaceDeclInfo>
foreign func clang_index_getObjCCategoryDeclInfo(_: CPointer<CXIdxDeclInfo>): CPointer<CXIdxObjCCategoryDeclInfo>
foreign func clang_index_getObjCProtocolRefListInfo(_: CPointer<CXIdxDeclInfo>): CPointer<CXIdxObjCProtocolRefListInfo>
foreign func clang_index_getObjCPropertyDeclInfo(_: CPointer<CXIdxDeclInfo>): CPointer<CXIdxObjCPropertyDeclInfo>
foreign func clang_index_getIBOutletCollectionAttrInfo(_: CPointer<CXIdxAttrInfo>): CPointer<CXIdxIBOutletCollectionAttrInfo>
foreign func clang_index_getCXXClassDeclInfo(_: CPointer<CXIdxDeclInfo>): CPointer<CXIdxCXXClassDeclInfo>
foreign func clang_index_getClientContainer(_: CPointer<CXIdxContainerInfo>): CXIdxClientContainer
foreign func clang_index_setClientContainer(_: CPointer<CXIdxContainerInfo>, _: CXIdxClientContainer): Unit
foreign func clang_index_getClientEntity(_: CPointer<CXIdxEntityInfo>): CXIdxClientEntity
foreign func clang_index_setClientEntity(_: CPointer<CXIdxEntityInfo>, _: CXIdxClientEntity): Unit


foreign func clang_IndexAction_create(CIdx: CXIndex): CXIndexAction
foreign func clang_IndexAction_dispose(_: CXIndexAction): Unit

foreign func clang_indexSourceFile(
    _: CXIndexAction,
    client_data: CXClientData,
    index_callbacks: CPointer<IndexerCallbacks>,
    index_callbacks_size: UInt32,
    index_options: UInt32,
    source_filename: CString,
    command_line_args: CPointer<CString>,
    num_command_line_args: Int32,
    unsaved_files: CPointer<CXUnsavedFile>,
    num_unsaved_files: UInt32,
    out_TU: CPointer<CXTranslationUnit>,
    TU_options: UInt32
): Int32
@FromClang["3.8"]
foreign func clang_indexSourceFileFullArgv(
    _: CXIndexAction,
    client_data: CXClientData,
    index_callbacks: CPointer<IndexerCallbacks>,
    index_callbacks_size: UInt32,
    index_options: UInt32,
    source_filename: CString,
    command_line_args: CPointer<CString>,
    num_command_line_args: Int32,
    unsaved_files: CPointer<CXUnsavedFile>,
    num_unsaved_files: UInt32,
    out_TU: CPointer<CXTranslationUnit>,
    TU_options: UInt32
): Int32
foreign func clang_indexTranslationUnit(
    _: CXIndexAction,
    client_data: CXClientData,
    index_callbacks: CPointer<IndexerCallbacks>,
    index_callbacks_size: UInt32,
    index_options: UInt32,
    _: CXTranslationUnit
): Int32
foreign func clang_indexLoc_getFileLocation(
    loc: CXIdxLoc,
    indexFile: CPointer<CXIdxClientFile>,
    file: CPointer<CXFile>,
    line: CPointer<UInt32>,
    column: CPointer<UInt32>,
    offset: CPointer<UInt32>
): Unit
foreign func clang_indexLoc_getCXSourceLocation(loc: CXIdxLoc): CXSourceLocation

@FromClang["3.7"]
foreign func clang_Type_visitFields(T: CXType, visitor: CXFieldVisitor, client_data: CXClientData): UInt32
