# Clang bindings for Cangjie

There are **libclang** idiomatic bindings for cangjie.

It is possible to use both "raw" and "idiomatic" bindings with different in convenient wrappers for the latter.

We try to provide API compatibility with clang version you require. Versions alternatives are supported as `cjc`/`cjpm` 
conditional variables ("features").
To compile with a specific clang version, append `--condition="clang_10_0"` to your cjpm's compile arguments (or if you use cjc, append `--conditional-compilation-config="(clang=10.0)"`). It's important to use underscores (`_`) in cjpm options.

## Usage

### Idiomatic way

Currently, not all struct wrappers are done. 
Already implemented wrappers:
- `TranslationUnit`
- `Index`
- `Cursor`
- `CursorSet`
- `SourceLocation`
- `SourceRange`
- `UnsavedFile`
- `CFile`

Another wrappers will be added as soon as possible.

```swift
from clang import libclang.Index
from clang import libclang.Cursor
from clang import libclang.TranslationUnit
from clang import libclang.TranslationUnitFlags
from clang import libclang.ChildVisitResult

func example() {
    try (index = Index(false)) {
        try(tunit = TranslationUnit.parse(index, "test/data/header.h", TranslationUnitFlags.CXTranslationUnit_SkipFunctionBodies)) {
            let cursor = tunit.getCursor()
            cursor.visitChildren { cursor: Cursor, _: Cursor =>
                println(cursor)
                ChildVisitResult.CXChildVisit_Continue
            }
        }
    }
}
```

### Raw calls

All bindings has "safe" wrappers over unsafe functions, there is no need to add `unsafe {}` blocks on yourself.

```swift
from clang import libclang.raw.*
from clang import libclang.GlobalOptFlags
from clang import libclang.getString
from clang import libclang.ChildVisitResult


func example_raw() {
    let index = createIndex(false, false)
    CXIndex_setGlobalOptions(index, GlobalOptFlags.CXGlobalOpt_ThreadBackgroundPriorityForIndexing.ordinal)

    let csourceFile = unsafe { LibC.mallocCString("test/data/header.h") }
    let tunit = parseTranslationUnit(index, csourceFile, CPointer(), 0, CPointer(), 0, 0)

    let tunitCursor = getTranslationUnitCursor(tunit)

    let visitor: CFunc<(CXCursor, CXCursor, CXClientData) -> UInt32> = { cursor, parent, data =>
        println(getString(cursor) { c => getCursorDisplayName(c) })
        ChildVisitResult.CXChildVisit_Continue.ordinal
    }

    visitChildren(tunitCursor, visitor, CPointer())

    disposeTranslationUnit(tunit)
    unsafe { LibC.free(csourceFile) }
    disposeIndex(index)
}
```

## Supported clang versions as conditional compilation variable
To compile with suitable major clang version, use one of possible variants below. Major version includes every possible
minor versions, such as if you have `clang 16.0.4`, specify `16.0` conditional variable version.

Supported clang versions:

- 3.5
- 3.6
- 3.7
- 3.8
- 3.9
- 4.0
- 5.0
- 6.0
- 7.0
- 8.0
- 9.0
- 10.0
- 11.0
- 12.0
- 13.0
- 14.0
- 15.0
- 16.0
- 17.0

## Build

For successful build it is required to specify:
- Path to `libclang.so` via `LIBRARY_PATH=/path/to/clang/lib`
- Current clang version via `--condition="clang_xx_xx"`

Example: `LIBRARY_PATH=/path/to/clang/lib cjpm build --condition="clang_15_0"`

### Test

1. `cjpm clean`
2. `LIBRARY_PATH=/path/to/clang/lib cjpm test --no-run --condition="clang_xx_xx"`
3. `LIBRARY_PATH=/path/to/clang/lib cjpm test --skip-build --condition="clang_xx_xx"`

Or just `LIBRARY_PATH=/path/to/clang/lib cjpm test --condition="clang_xx_xx"`

## Suggestions, questions, bugs and any other feedback

If you are interested in this library or have any questions or issues, you are welcome to create the issue/pullrequest in this repo.

Or, feel free to contact with me Weelink: `sWX1303318`